import Scorm from "../../core/Scorm";
import { ScormException } from "../../core/types/Exceptions";
import { IOptionsDefaultScorm, IOptionsDefaultScormV2004 } from "../../core/types/IOptionsDefaultScorm";

export default abstract class V2004OptionsDefault extends Scorm implements IOptionsDefaultScorm, IOptionsDefaultScormV2004 {
    public getLearner(): any {
        var learner = {
            id: "",
            name: ""
        };
        learner.name = this.getData("cmi.learner_name");
        learner.id = this.getData("cmi.learner_id");
        return learner;
    }

    public getLocation(): string {
        return this.getData("cmi.location");
    }
    public setLocation(value: string): boolean {
        return this.setData("cmi.location", value);
    }

    public getScoreRaw(): string {
        return this.getData("cmi.score.raw");
    }
    public setScoreRaw(value: string): boolean {
        return this.setData("cmi.score.raw", value);
    }
    public getScoreMax(): string {
        return this.getData("cmi.score.max");
    }
    public setScoreMax(value: string): boolean {
        return this.setData("cmi.score.max", value);
    }
    public getScoreMin(): string {
        return this.getData("cmi.score.min");
    }
    public setScoreMin(value: string): boolean {
        return this.setData("cmi.score.min", value);
    }
    public getCompletionStatus(): string {
        return this.getData("cmi.completion.status");
    }
    public setCompletionStatus(value: string): boolean {
        var value_allowed = [
            "completed", "incomplete", "not attempted", "unknown"
        ];
        if (!value_allowed.includes(value)) {
            throw new ScormException("CompletionStatus value not supported, use " + value_allowed.join(","));
        }
        return this.setData("cmi.completion_status", value);
    }

    public setScoreScaled(value: string): boolean {
        return this.setData("cmi.score.scaled", value);
    }
    public getScoreScaled(): string {
        return this.getData("cmi.score.scaled");
    }

    public setSuccessStatus(value: string): boolean {

        var value_allowed = [
            "passed", "failed", "unknown"
        ];
        if (!value_allowed.includes(value)) {
            throw new ScormException("CompletionStatus value not supported, values allowes: " + value_allowed.join(","));
        }

        return this.setData("cmi.success.status", value);
    }
    public getSuccessStatus(): string {
        return this.getData("cmi.success_status");
    }
}