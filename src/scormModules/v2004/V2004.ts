
import { Version } from "../../core/types/Version";
import V2004OptionsDefault from "./V2004OptionsDefault";
import { ApiScormConectionFunction } from "../../core/connection/ApiScormConection";
import OptionsParams from "../../core/types/OptionsParams";

export default class V2004 extends V2004OptionsDefault{

    public constructor(windowContext:any, options:OptionsParams){
        super(windowContext, options);
        this.context = this.getContextApi();
    }

    public getVersion(): Version {
        return Version.v2004;
    }
    public functionNames(): ApiScormConectionFunction {
        return {
            initialize: "Initialize",
            terminate: "Terminate",
            setvalue: "SetValue",
            getvalue: "GetValue",
            commit: "Commit",
        };
    }
}