import ScormBase from "../../core/ScormBase";
import Scorm from "../../core/Scorm";

import { Tools } from "../../core/Tools";
import { ScormException } from "../../core/types/Exceptions";
import { Version } from "../../core/types/Version";
import V12OptionsDefault from "./V12OptionsDefault";
import { ApiScormConectionFunction } from "../../core/connection/ApiScormConection";
import OptionsParams from "../../core/types/OptionsParams";

export default class V12 extends V12OptionsDefault{

    public constructor(windowContext:any, options:OptionsParams){
        super(windowContext, options);
        this.context = this.getContextApi();
    }

    public getVersion(): Version {
        return Version.v12;
    }
    public functionNames(): ApiScormConectionFunction {
        return {
            initialize: "LMSInitialize",
            terminate: "LMSFinish",
            setvalue: "LMSSetValue",
            getvalue: "LMSGetValue",
            commit: "LMSCommit"
        };
    }
}