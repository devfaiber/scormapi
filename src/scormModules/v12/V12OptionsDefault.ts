import Scorm from "../../core/Scorm";
import { ScormException } from "../../core/types/Exceptions";
import { IOptionsDefaultScorm } from "../../core/types/IOptionsDefaultScorm";

export default abstract class V12OptionsDefault extends Scorm implements IOptionsDefaultScorm {
    public getLearner(): any {
        var learner = {
            id: "",
            name: ""
        };
        learner.name = this.getData("cmi.core.student_name");
        learner.id = this.getData("cmi.core.student_id");

        return learner;
    }

    public getLocation(): string {
        return this.getData("cmi.core.lesson_location");
    }
    public setLocation(value: string): boolean {
        return this.setData("cmi.core.lesson_location", value);
    }

    public getScoreRaw(): string {
        return this.getData("cmi.core.score.raw");
    }
    public setScoreRaw(value: string): boolean {
        return this.setData("cmi.core.score.raw", value);
    }
    public getScoreMax(): string {
        return this.getData("cmi.core.score.max");
    }
    public setScoreMax(value: string): boolean {
        return this.setData("cmi.core.score.max", value);
    }
    public getScoreMin(): string {
        return this.getData("cmi.core.score.min");
    }
    public setScoreMin(value: string): boolean {
        return this.setData("cmi.core.score.min", value);
    }
    public setCompletionStatus(value: string): boolean {
        var value_allowed = [
            "completed", "incomplete", "not attempted", "browsed", "not_attempted"
        ];
        if (!value_allowed.includes(value)) {
            throw new ScormException("CompletionStatus value not supported, use " + value_allowed.join(","));
        }
        return this.setData("cmi.core.lesson_status", value);
    }
    public getCompletionStatus(): string {
        return this.getData("cmi.core.lesson_status");
    }
}