
import { Version } from "./core/types/Version";
import { VersionException } from "./core/types/Exceptions";
import OptionsParams from "./core/types/OptionsParams";
import V12 from "./scormModules/v12/v12";
import Scorm from "./core/Scorm";
import V2004 from "./scormModules/v2004/V2004";
import { DebuggerScorm } from "./core/types/DebuggerScorm";


export class ScormApi {
    private _options: OptionsParams;
    private _windowContext: any;
    public constructor(windowContext: any, options?: OptionsParams) {
        this._options = options;
        this._windowContext = windowContext;
    }

    public build(): Scorm {
        DebuggerScorm.DEBUG = this._options.debug ?? false;
        return this._getFeatureInstance(this._options?.version ?? Version.v12);
    }

    private _getFeatureInstance(version: Version): V12 | V2004 {
        if (version == Version.v12) {
            return new V12(this._windowContext, this._options);
        } else if (version == Version.v2004) {
            return new V2004(this._windowContext, this._options);
        } else {
            throw new VersionException("Unsupported version: " + version);
        }
    }
}
