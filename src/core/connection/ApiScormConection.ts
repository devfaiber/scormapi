export class ApiScormConection implements IApiScormConnection {
    
    public api:any; // api encontrada
    public functionsName:ApiScormConectionFunction;
    public isConnected:boolean;

    public constructor(api:any, functions:ApiScormConectionFunction) {
        this.api = api;
        this.functionsName = functions;
    }

    public initializeLMS(value:string): string {
        this.isConnected = this.api.isConnected;

        return this.api[this.functionsName.initialize](value);
    }
    public terminateLMS(value:string): string {

        let success = this.api[this.functionsName.terminate](value);
        this.isConnected = this.api.isConnected;
        return success;
    }
    public setValueLMS(param:string, value:string): string {
        return this.api[this.functionsName.setvalue](param, value);
    }
    public getValueLMS(value:string): string {
        return this.api[this.functionsName.getvalue](value);
    }
    public commitLMS(value:string): string {
        return this.api[this.functionsName.commit](value);
    }
}

export interface IApiScormConnection {
    initializeLMS(value:string): string;
    terminateLMS(value:string): string;
    setValueLMS(param:string, value:string): string;
    getValueLMS(value:string): string;
    commitLMS(value:string): string;
}

export class ApiScormConectionFunction{
    initialize:string;
    terminate:string;
    setvalue:string;
    getvalue:string;
    commit: string;
}