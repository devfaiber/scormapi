

export default class ScormFind implements IScormFind {

    public windowContext: any;
    public propertyApi: string;
    public MAX_TREE_PARENT:number = 2;

    public constructor(windowContext: any, propertyApi: string) {
        this.windowContext = windowContext;
        this.propertyApi = propertyApi;
    }

    public findContextWithApi(): any {

        return this.search([
            this.searchToContext,
            this.searchToOpener
        ]);

    }

    public search(callbacksSearcher: any[]) {
        let contextWithApi;
        for (var searcher of callbacksSearcher) {
            contextWithApi = searcher.call(this);
            if (contextWithApi != null) {
                break;
            }
        }
        return contextWithApi;
    }

    public searchToOpener() {
        if(window.opener == null){
            return null;
        }
        return this.searchTreeToParent(this.MAX_TREE_PARENT, this.windowContext.opener)
    }
    public searchToContext() {
        return this.searchTreeToParent(this.MAX_TREE_PARENT, this.windowContext);
    }
    private searchTreeToParent(level:number, winContext: any): any {
        let api;
        let context = winContext; // selecciona el actual window

        for (let i = 0; i < level; i++) {

            api = context[this.propertyApi];
            if (api != null) {
                return context;
            }

            // selecciona el padre
            if(context == parent){
                return null;
            }
            context = context.parent;
        }

        return null;
    }
    
}
interface IScormFind {
    findContextWithApi(): any
}