export class VersionException extends Error {
    public constructor(public message: string) {
        super(message);
    }
}

export class ScormException extends Error {
    public constructor(public message: string) {
        super(message);
    }
}