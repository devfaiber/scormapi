export class DebuggerScorm {

    public static DEBUG:boolean = false;

    public static write(value: string): void {
        if(this.DEBUG){
            console.log(`SCORM-CALL: ${value}`);
        }
    }
    public static writeError(value: string): void {
        if(this.DEBUG){
            console.log(`SCORM-ERROR: ${value}`);
        }
    }
    public static writeErrorAlways(value: string): void {
        console.log(`SCORM-ERROR: ${value}`);
    }
}