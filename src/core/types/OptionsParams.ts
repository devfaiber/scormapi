import { Version } from "./Version";

export default class OptionsParams {
    version?: Version;
    debug?: boolean;
    saveConnectionInParentWindow?: boolean;
    saveConnectionInOpenerWindow?: boolean;
}