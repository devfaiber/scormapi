export interface IOptionsDefaultScorm {

    getLearner(): any;
    getLocation(): string;
    setLocation(value: string): boolean;

    getScoreRaw(): string ;
    setScoreRaw(value: string): boolean;
    getScoreMax(): string;
    setScoreMax(value: string): boolean;
    getScoreMin(): string;
    setScoreMin(value: string): boolean;
    getCompletionStatus(): string
    setCompletionStatus(value: string): boolean;
}

export interface IOptionsDefaultScormV2004 {
    setScoreScaled(value: string): boolean;
    getScoreScaled(): string;
    getSuccessStatus(): string
    setSuccessStatus(value: string): boolean;
}