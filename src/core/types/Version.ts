export enum Version {
    v2004="v2004",
    v12="v12",
}

export abstract class PropertyWinContextVersion {


    public static getProperty(version:Version) : string {
        if(version == Version.v12){
            return "API";
        }else if(version == Version.v2004){
            return "API_1484_11";
        }
    }
    public static getAllVersions(){
        return [
            "API",
            "API_1484_11",
        ];
    }
}