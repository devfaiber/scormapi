export class Tools {
    public static convertToBoolean(value: string): boolean {
        switch(value){
            case "true":
            case "yes":
            case "1":
                return true;
            default:
                return false;
        }
    }
}