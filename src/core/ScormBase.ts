import { DebuggerScorm } from "./types/DebuggerScorm";
import { ScormException } from "./types/Exceptions";

export default abstract class ScormBase{

    // contiene el context de donde se encuentra la api. puede ser por ejemplo
    // window.opener
    // window.parent
    // window
    // este objeto muta tener en cuenta
    protected context: any;
    protected _contextOriginal: any;

    public isFoundApi: boolean;
    public connection: any = {};

    public constructor(windowContext:any){
        this.context = windowContext;
        this._contextOriginal = windowContext;
    }
    public getData(scormParam: string): string{
        if(!this.connection.isConnected){
            DebuggerScorm.writeErrorAlways("connection not connected");
        }

        return this._getValueLMS(scormParam);
    }
    public setData(scormParam: string, data: any):boolean{
        if(!this.connection.isConnected){
            DebuggerScorm.writeErrorAlways("connection not connected");
        }

        return this._setValueLMS(scormParam, data);
    }

    public initialize(): boolean {
        if(this.connection.isConnected){
            DebuggerScorm.write("openConnectionLMS connection already connected");
        }

        var success = this._openConnectionLMS();
        if(success){
            this.connection.ApiVersion = this.getApiHandle();
            this.connection.isConnected = true;
        } else {
            this.connection.isConnected = false;
        }
        return success;
    }
    public terminate() : boolean {
        if(!this.connection.isConnected){
            DebuggerScorm.write("closeConnectionLMS connection already not connected");   
        }
        let success = this._closeConnectionLMS();
        if(success){
            this.connection.isConnected =  false;
        }
        return success;
    }
    public save() : boolean {
        if(!this.connection.isConnected){
            DebuggerScorm.writeErrorAlways("connection not connected");
        }
        let success = this._saveLMS();

        return success;
    }

    public getContextApi(){
        return this.context;
    }
    protected abstract getApiHandle(): any;
    protected abstract _getValueLMS(scormKey:string): string;
    protected abstract _setValueLMS(scormKey:string, value:string):boolean;
    protected abstract _openConnectionLMS(): boolean;
    protected abstract _closeConnectionLMS(): boolean;
    protected abstract _saveLMS(): boolean;
}