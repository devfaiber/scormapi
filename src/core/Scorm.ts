import { ApiScormConection, ApiScormConectionFunction } from "./connection/ApiScormConection";
import ScormBase from "./ScormBase";
import ScormFind from "./ScormFind";
import { Tools } from "./Tools";
import { DebuggerScorm } from "./types/DebuggerScorm";
import { ScormException } from "./types/Exceptions";
import OptionsParams from "./types/OptionsParams";
import { PropertyWinContextVersion, Version } from "./types/Version";

export default abstract class Scorm extends ScormBase {

    private scormFind: ScormFind;
    private propertyApiContext: string;

    private options?: OptionsParams;

    public constructor(winContext: any, options?: OptionsParams) {
        super(winContext);
        this.options = options ?? {};
        this.propertyApiContext = PropertyWinContextVersion.getProperty(this.getVersion());
        this.scormFind = new ScormFind(winContext, this.propertyApiContext);
    }
    public getContextApi() {
        let context = this.scormFind.findContextWithApi();
        return context;
    }

    

    protected _getValueLMS(scormKey: string): string {

        let value = this.getApiHandle().getValueLMS(scormKey) ?? "";
        DebuggerScorm.write(`GetValueLMS value ${scormKey}: ${value}`);
        return value;
    }
    protected _setValueLMS(scormKey: string, value: string): boolean {
        let success = Tools.convertToBoolean(this.getApiHandle().setValueLMS(scormKey, value));
        DebuggerScorm.write(`SetValueLMS value ${scormKey}: ${success}`);
        return success;
    }

    protected _openConnectionLMS(): boolean {
        let success = Tools.convertToBoolean(this.getApiHandle().initializeLMS(""));;
        DebuggerScorm.write(`OpenConnectionLMS: ${success}`);
        return success;
    }
    protected _closeConnectionLMS(): boolean {
        let success = Tools.convertToBoolean(this.getApiHandle().terminateLMS(""));
        DebuggerScorm.write(`CloseConnectionLMS: ${success}`);
        return success;
    }
    protected _saveLMS(): boolean {
        let success = Tools.convertToBoolean(this.getApiHandle().commitLMS(""));
        DebuggerScorm.write(`CommitLMS: ${success}`);
        return success;
    }


    public getApiHandle(): ApiScormConection {

        // si no hay context vuelve a verificar o aplica por defecto el actual
        if(this.context == null){
            this.context = this.getContextApi() ?? window;
        }

        var apiConnection:ApiScormConection = this._getApiSavedConnection();
        if (!apiConnection) {
            apiConnection = new ApiScormConection(this._getApi(), this.functionNames());
            this._saveApiConnectionInParentContext(apiConnection);
        }
        return apiConnection;
    }
    private _getApi(): any {
        let api = this.context ? this.context[this.propertyApiContext] : null;
        if (api == null) {
            DebuggerScorm.write(`getApiHandle not found API with property "${this.propertyApiContext}"`);
            throw new ScormException(`property ${this.propertyApiContext} not found en wincontext`);
        } else {
            DebuggerScorm.write(`getApiHandle found API with property ${this.propertyApiContext}`);
            this.isFoundApi = true;
        }

        return api;
    }
    private _saveApiConnectionInParentContext(apiConnection: ApiScormConection): void {
        if (this.options != null && this.options.saveConnectionInParentWindow) {

            if (this.context == window) {
                throw new ScormException('options[saveConnectionInParentWindow] is not supportes, windowcontext is equal to parent');
            }
            this._contextOriginal.parent.apiConnection = apiConnection;
            DebuggerScorm.write(`saveConnectionInParentWindow: true`);
        } else if (this.options != null && this.options.saveConnectionInOpenerWindow) {
            if (this.context == null || this.context == window) {
                throw new ScormException('options[saveConnectionInOpenerWindow] is not defined or is equal to current windowcontext');
            }
            this._contextOriginal.opener.apiConnection = apiConnection;
            DebuggerScorm.write(`saveConnectionInOpenerWindow: true`);
        }
    }
    private _getApiSavedConnection(): ApiScormConection{
        if(this.options.saveConnectionInParentWindow){
            return this.context.apiConnection;
        } else if(this.options.saveConnectionInOpenerWindow){
            return this.context.apiConnection; //el context is window.top
        }
        return null;
    }

    public abstract getVersion(): Version;
    public abstract functionNames(): ApiScormConectionFunction;
}