# Description
esta libreria permite establecer conexiones SCORM entre un sition y un paquete scorm v1.2 o v2004.

# Compilacion
para realizar la compilacion se puede hace mediante la descarga del proyecto. y posteriomente se puede correr:

para desarrollo:
```
npm run build
```

para produccion:
```
npm run build-prod
```

# funcionamiento
la libreria funciona basa de API_Wrapper escribiendo todo el codigo de cero. y eliminando informacion no utilizada. sin embargo esta en fase de pruebas por el momento.

esta libreria maneja el nivel de contextos asociado al objeto window. mediante una busqueda de arbol. localiza la api del objeto window sea que este en un nivel muy alto o no. conseguira encontrarla. asi que cuando se refiere a contextos, maneja es un objeto window ya sea una window.parent o un window.opener

# ejemplo para uso
se puede agregar y utilizar facilmente.

debe cargar el bundle generado
```
<script src="dist/bundle.js"></script>
```
posteriomente se puede instanciar la clase construyendo una instancia. para poder acceder a los metodos de escritura
```
<script>
    var scorm = ScormApi.create().build();
</script>
```

## optiones
puede  pasarse diferentes opciones
```
var scorm = ScormApi.create({
        version: 'v2004',
        debug: true,
        saveConnectionInParentWindow: true
    }).build();
```


| propiedad | descripcion |
|-----------|--------------|
| version   | por defecto maneja la version 1.2 o `v12` permite el uso de `v2004`  |
| debug | permite devolver texto en consola para depurar valores que se obtengan |
| saveConnectionInParentWindow | permite guardar la conexion en el window.parent para poder reutilizarla cuando se tiene multiple pagina que recarga, **Requiere un iframe interno**
|saveConnectionInOpenerWindow| guarda la conexion en window.opener para poder reutilizarla **Requiere que la ventana haya sido abierta por un evento de boton**
